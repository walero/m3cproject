"""
Prasun Ray
Solution, Homework 4, part 1
Test gradient routines in fortran module, fdmodule2d
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 hw41soln.f90 -m hw4 -llapack
"""
from hw4 import fdmodule as f1
from hw4 import fdmodule2d as f2d
import numpy as np
import matplotlib.pyplot as plt
#----------------------------------------------   

def test_grad1(n1,n2,numthreads):
    """obtain errors and timing values from test_grad and test_grad_omp
    return the two errors and average times from ten calls to the test routines
    Input: n1: number of points in x1
           n2: number of points in x2
           numthreads: number of threads used in test_grad_omp
    """

    #set fortran module variables    
    f2d.n1=n1
    f2d.n2=n2

    f2d.dx1=1.0/(f2d.n1-1)
    f2d.dx2=1.0/(f2d.n2-1)

    f2d.numthreads = numthreads
    #-----------

    tarray = []
    tparray = []    

    #call fortran test routines ten times and return error and average times    
    for i in range(10):
        e,t = f2d.test_grad()
        ep,tp=f2d.test_grad_omp()

        tarray += [t]
        tparray += [tp]
    

    return e,ep,np.mean(tarray),np.mean(tparray)    
#----------------------------------------------   
def test_gradN(numthreads):
    """input: number of threads used in test_grad_omp
    call test_grad1 with a range of grid sizes and
    assess speedup
    """
 
    n1values = np.array([50, 100, 200, 400, 800, 1600, 3200])
    n2values = n1values
    nn = np.size(n1values)
    speedup = np.empty((nn,nn))

    #loop through 50<n1,n2<1600 storing speedup
    for i in enumerate(n1values):
        for j in enumerate(n2values):
            print "running with n1,n2=",i[1],j[1]              
            f2d.n1=i[1]
            f2d.n2=j[1]
    
            e,ep,t,tp = test_grad1(f2d.n1,f2d.n2,2)
            speedup[i[0],j[0]] = t/tp #compute and store speedup

    #display results
    plt.figure()
    plt.plot(n1values,speedup[:,::2])
    plt.legend(('n2=50','200','800','3200'),loc='best')
    plt.xlabel('n1')
    plt.ylabel('speedup')
    plt.title('Prasun Ray, test_gradN')
    plt.axis('tight')
    plt.savefig("hw411.png")            
    return speedup
    
#----------------------------------------------   

if __name__ == "__main__":
    
    e,ep,t,tp = test_grad1(400,200,2)
    print "Results from test_grad1, n1=400,n2=200,numthreads=2"
    print "e,t=",e,t
    print "ep,tp=",ep,tp
    print "checking ratio e[1]/e[0]:",e[1]/e[0]
    s = test_gradN(2) #modify if anything is being returned
    plt.show()