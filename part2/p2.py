"""Project part 2
Use gradient thresholding to process image.
Gradient is computed using grad_omp routine in fdmodule2d
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 fdmodule2d.f90 -m p2 -llapack
"""
from p2 import fdmodule2d as f2d
from scipy import misc
import numpy as np
import matplotlib.pyplot as plt

def threshold(M,fac,display,savefig,debug=False):
	"""set all elements of M to zero where grad_amp < fac*max(grad_amp)
	added: assert statement for fac to ensure it is non-negative since df_amp
		is non-negative we require fac to be positive to get some thresholding
		due to the equation we use
	added: assert statement on dimension of input array, M since we require three 
		dimensions each representing a colour
	added: asset statement on elements of input array M being postive since in
		question it states numbers representing colours range from 0.0-1.0
		**note** i would have made assert (0<=M).all() & (1>=M)==True though
		but dtype=uint8 M=misc.face() between 0-225, which confused me?
	added: debug statements are such that if debug is set to true, it checks if
		any thresholding is actually occuring and breaks if none is. 
		additionally breaks dfamp and dfmax for checking
	"""
	
	assert fac>=0, "error: input fac must be non-negative to apply thresholding" 
	assert np.shape(M)[2]==3, "error: input array must be three dimensional (each \
		dimension representing a colour red, blue, green)"
	assert (0<=M).all()==True, "error: input array must be non-negative"

	f2d.dx1=1
	f2d.dx2=1
	f2d.n1=np.shape(M)[1]
	f2d.n2=np.shape(M)[0]
	dfamp=np.zeros(np.shape(M))
	dfmax=np.zeros(3)
	if debug:
			M_debug=np.copy(M)
	for i in range(0,3):
		dfamp, dfmax[i]=f2d.grad_omp(M[:,:,i])[2:4]
		#apply gradient thresholding to each colour
		M[:,:,i]=M[:,:,i]-np.where(dfamp<fac*dfmax[i],M[:,:,i],0)
		#note: 
		#np.where(i,ii,iii) works by changing any elements in array (ii) which do not
		#have the same postion of elements an array (i) meeting a condition (i.e. 
		#boolean involving array (i) : example (i)=0) to the value corresponding to 
		#(iii) hence the reason we are taking away the output from M[:,:,i] for
		#thresholding
		if debug:
			print "after applying thresholding to colour %d" %(i+1)
			print "dfamp,dfmax=$", dfamp[i],dfmax[i]
			if (M[:,:,i]==M_debug[:,:,i]).all():
				print "no thresholding occured"
				break
	if display==True:
		plt.figure()
		plt.imshow(M)
	if savefig==True:
		plt.savefig("p2.png")

	return M,dfmax

if __name__ == '__main__':
	M=misc.face()
	print "shape(M):",np.shape(M)
	plt.figure()
	plt.imshow(M)
	plt.show() #may need to close figure for code to continue
	#plt.close()
	N,dfmax=threshold(M,0.2,True,True) #Uncomment this line
	plt.show()