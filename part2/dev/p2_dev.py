"""Project part 2
Use gradient thresholding to process image.
Gradient is computed using grad_omp routine in fdmodule2d
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 fdmodule2d.f90 -m p2 -llapack
"""
from p2 import fdmodule2d as f2d
from scipy import misc
import numpy as np
import matplotlib.pyplot as plt

def threshold(M,fac,display,savefig):
	"""set all elements of M to zero where grad_amp < fac*max(grad_amp)
	"""	
	f2d.dx1=1
	f2d.dx2=1
#   df1=np.zeros(3)
#	df2=np.zeros(3)
	dfamp=np.zeros(3)
    dfmax=np.zeros(3)
    for i in range(0,3):
#    	df1[i], df2[i]=f2d.grad_omp(M[:,:,i])[0:1]
		dfamp[i], dfmax[i]=f2d.grad_omp(M[:,:,i])[2:3]
		M[:,:,i]=M[:,:,i]-np.where(dfamp[i]<fac*dfmax[i],M[:,:,i],0)
	return M,dfmax

if __name__ == '__main__':
    M=misc.face()
    print "shape(M):",np.shape(M)
    plt.figure()
    plt.imshow(M)
    plt.show() #may need to close figure for code to continue
    N,dfmax=threshold(M,0.2,True,True) #Uncomment this line 
    plt.show()
    