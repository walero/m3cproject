#Project part 4
import numpy as np
import matplotlib.pyplot as plt
import adv2d
#gfortran -c advmodule2d.f90
#f2py -llapack -c advmodule2d.f90 fdmoduleB.f90 fdmodule2dp.f90 ode2d.f90 -m adv2d --f90flags='-fopenmp' -lgomp

def advection2d(nt,n1,n2,tf=1.0,c1=1.0,c2=1.0,S=0,display=False,par=0,numthreads=1):
	"""solves the two-dimenisonal advection equation with initial condition
		f0=np.exp(-100*(((xx-0.5)**2)+((yy-0.5)**2))) and displays contour plot
		for any input of timw, tf and returns the solution, walltime and error
		with respect to the exact solution (can also work in parallel)
	"""
	dx=1./n1
	dy=1./n2
	x=np.arange(0.0,n1*dx,dx)
	y=np.arange(0.0,n2*dy,dy)
	#xx=np.zeros((n2,n1))
	#yy=np.zeros((n2,n1))
	#for i in range(0,n1):
	#	xx[i,:]=x
	#for j in range(0,n2):
	#	yy[:,j]=y
	xx,yy=np.meshgrid(x,y)
	dt=float(tf)/nt

	f0=np.exp(-100*(((xx-0.5)**2)+((yy-0.5)**2)))
	f_exact=np.exp(-100*((((xx-c1*tf)%1-0.5)**2)+(((yy-c2*tf)%1-0.5)**2)))+S*tf
	#--------------
	adv2d.advmodule.c1_adv=c1
	adv2d.advmodule.c2_adv=c2
	adv2d.advmodule.s_adv=S
	adv2d.fdmodule2dp.n1=n1
	adv2d.fdmodule2dp.n2=n2
	adv2d.ode2d.numthreads=numthreads
	adv2d.ode2d.par=par
	adv2d.fdmodule2dp.dx1=dx
	adv2d.fdmodule2dp.dx2=dy
	f,t=adv2d.ode2d.rk4(0,f0,dt,nt)
	error=np.mean(np.abs(f-f_exact))

	if display==True:
		plt.figure()
		plt.contourf(x, y, f)
		plt.figure()
		plt.contourf(x, y, f_exact)


	return f,error,t

def test_advection2d(n1,n2,numthreads):
	"""test advection2d function with specfic nt(=800) but takes n1,n2 and numthreads
		as inputs, returns errors and walltime for both paralleliased and non
		paralleliased
	"""

	tarray = []
	tparray = []

	for i in range(1):
		f,e,t = advection2d(800,n1,n2,1.0,1.0,1.0,0,False,0)
		fp,ep,tp = advection2d(800,n1,n2,1.0,1.0,1.0,0,False,1,numthreads)

		tarray += [t]
		tparray += [tp]

	return e,ep,np.mean(tarray),np.mean(tparray)

def test_advection2dN(numthreads):
	"""assesses accuracy and speedup of paralleliased advection2d, returning an
		array of speedup and graphs showing error against grid size and speedup
		against grid size
	"""
	n1values = np.array([50, 100, 200, 400, 600])
	n2values = n1values
	nn = np.size(n1values)
	speedup = np.empty((nn,nn))
	error = np.empty((nn,nn))
	for i in enumerate(n1values):
		for j in enumerate(n2values):
			print "running with n1,n2=",i[1],j[1]
			adv2d.fdmodule2dp.n1=i[1]
			adv2d.fdmodule2dp.n2=j[1]
			e,ep,t,tp = test_advection2d(adv2d.fdmodule2dp.n1,adv2d.fdmodule2dp.n2,2)
			speedup[i[0],j[0]] = t/tp #compute and store speedup
			error[i[0],j[0]] = e

	plt.figure()
	plt.plot(n1values,speedup[:,::1])
	plt.legend(('n2$=50$','n2$=100$','n2$=200$','n2$=400$','n2$=600$'),loc='best')
	plt.xlabel('n1')
	plt.ylabel('speedup')
	plt.title('Wale Osikomaiya, test_advection2dN, speedup= $T_{s}/T_{p}$, numthreads=%d' %(numthreads))
	plt.axis('tight')
	plt.savefig("p4_4.png")

	plt.figure()
	plt.plot(n1values,error)
	plt.legend(('n2$=50$','n2$=100$','n2$=200$','n2$=400$','n2$=600$'),loc='best')
	plt.xlabel('n1')
	plt.ylabel('error')
	plt.title('Wale Osikomaiya, test_advection2dN, error against grid size, numthreads=%d' %(numthreads))
	plt.axis('tight')
	plt.savefig("p4_5.png")
	return speedup

def test_advection2dnt(n1,n2):
	"""assesses convergence of advection2d for gridsize inputs n1,n2
		returning a plot showing nt against error and stating what values
		of nt the solution begins to converge to the exact solution
	"""
	assert n1<=1200 and n2<=1200, "n1 and n2 need be no bigger than 1200 to explore trend"

	ntvalues=np.array([100,200,300,400,500,600,700,800,900,1000,1100,1200])
	error = []
	j=[-1]
	for i in enumerate(ntvalues):
		print "running with nt=",i[1]
		f,e,t = advection2d(i[1],n1,n2,1.0,1.0,1.0,0,False,1,4)
		error += [e]
	print error
	for i in range(12):
		if error[i]<=1: #i sent convegence to be when error<1
			j = [i] #the code checks when this occurs and then plots accordingly
			print "analyical solution for n1=%d, n2=%d begins to converge past nt=" %(n1,n2),ntvalues[i]
			break
	plt.figure()
	plt.plot(ntvalues[j[0]:12],error[j[0]:12])
	plt.xlabel('nt')
	plt.ylabel('error')
	plt.title('Wale Osikomaiya, test_advection2dnt, error against nt, n1=%d,n2=%d' %(n1,n2))
	plt.axis('tight')
	plt.savefig("p4_6.png")

if __name__ == "__main__":
	"""trends & dicussion:
	1. from plotting the advection solution i found that tf essentially moved the
		solution as you increase it from tf=1, thought i found a tipping point for
		the solution where it's stop coverging to the exact solution when tf is
		changed, this being tf>1.28
			this can be seen for the fairly stable and convergent solution i found
		when nt=450,n1=n2=500
	2. as expected when the gridsize increases the speedup and accuracy of 
		advection2d increases and this can been seen clearly with test_advection2dN(4)
		though it should be noted that unlike speedup, their is a maximum level
		of accuary depending on the gridsize, whereas speedup does not seem to plateau

	3. from test_advection2dnt(500,500) it can be seen cleary that the value of nt
		for which the solution starts to converge depends heavily on the gridsize
		and i created the function to show exactually when this occurs

	overall i find with large enough nt and gridsize part n1=n2=500 you get 
		a solution which is very close to the exact solution
	"""
	e,ep,t,tp=test_advection2d(500,400,2)
	advection2d(450,500,500,1.28,1.0,1.0,0,True)
	plt.savefig("p4_1.png")
	plt.show()
	advection2d(450,500,500,1.29,1.0,1.0,0,True)
	plt.savefig("p4_2.png")
	plt.show()
	s=test_advection2dN(4)
	plt.show()
	test_advection2dnt(500,500)
	plt.show()
	plt.savefig("p4_3.png")



