# README #

The repository should also contain a readme file with a list of all files in the repo that you created or modified and a very brief description of each file

*contents*
---------------------------------------------------------
---------------------------------------------------------
**part1:**
---------------------------------------------------------
**\dev** - directory containing development versions of code (this can be ignored and is not for marking)

**data.in** - file containing info to be read by program 'test' in fdmodule2d.f90

**fdmodule.f90** - Fortran module containing routines for differentiating an array of size N with 2nd order and 4th order-compact finite differences

**fdmodule2d.f90** - Fortran module containing routines for differentiating n2 x n1 matrix with 2nd order and 4th order compact finite differences

**p1_3.py** - Python code that tests gradient routines in fortran module, fdmodule2d


**part2:**
---------------------------------------------------------
**\dev** - directory containing development versions of code (this can be ignored and is not for marking)

**fdmodule.f90** - Fortran module containing routines for differentiating an array of size N with 2nd order and 4th order-compact finite differences

**fdmodule2d.f90** - Fortran module containing routines for differentiating n2 x n1 matrix with 2nd order and 4th order compact finite differences

**p2.py** - Python code which uses gradient thresholding to process image, where Gradient is computed using grad_omp routine in fdmodule2d

**p2.png** - png file of raccoon (M=misc.face()) after applying gradient thresholding with threshold() python function, fac=0.2


**part3:**
---------------------------------------------------------
**\dev** - directory containing development versions of code (this can be ignored and is not for marking)

**data.in** - file containing info to be read by program 'test_adv' in advmodule.f90

**data_mpi.in** - file containing info to be read by program 'advection_mpi' in ode_pi.f90

**advmodule.f90** - Fortran module contains parameters for advection equation and main program tests that Euler integration of adv. eqn.

**ode_mpi.f90** - Fortran module contains parameters for advection equation and main program that tests Euler integration of adv. eqn. in parallel using MPI

**adv_mpi** - executable of Fortran program in ode_mpi.f90

**fmpi.dat** - solution of advection equation from Euler integration in parallel written by executable adv_mpi

**fdmoduleB.f90** - Fortran module containing routines for differentiating an array of size N with 2nd order and 4th order-compact finite differences, test routine test_fd that returns the error while test_fd_time returns timing information

**ode.f90** - Fortran module that uses RK4 or Euler, time marching to solve an initial value problem

**p3.py** - Python code that solves the 1d advection equation using Fortran routines producing a graph of solution and computing L1 error


**part4:**
---------------------------------------------------------
**fdmoduleB.f90** - Fortran module containing routines for differentiating an array of size N with 2nd order and 4th order-compact finite differences

**fmodule2d.f90** - Fortran module containing routines for differentiating n2 x n1 matrix with 2nd order and 4th order compact finite differences (both parallel and non-parallel)

**fmodule2dp.f90** - Fortran module similar to fmodule2d.f90 (calls fd2 instead of cfd4)

**advmodule2d.f90** - Fortran module contains parameters for advection equation

**ode2d.f90** - Fortran module to use RK4 time marching to solve an initial value problem Solves: dy/dt = RHS(y,t) where y is a 2d matrix

**adv2d.so** - f2py generated .so file from f2py -llapack -c advmodule2d.f90 fdmoduleB.f90 fmodule2dp.f90 ode2d.f90 -m adv2d --f90flags='-fopenmp' -lgomp

**p4.py** - Python code which solves the two-dimenisonal advection equation with specific initial condition, tests RK4 time marching, assesses accuracy and speedup of parallelised advection2 and assesses convergence of advection2d for grid size

**p4_1.png** - solution to 2d advection equation, contour graph

**p4_2.png** - solution to 2d advection equation, contour graph with change in tf

**p4_3.png** - error solution 2d advection equation against nt, n1=n2=500

**p4_4.png** - speedup adevctoin2d against gridsize 

**p4_5.png** - error adevctoin2d against gridsize 