!Project part 3
!module to use RK4 or Euler time marching to solve an initial value problem
!Solves: dy/dt = RHS(y,t)
module ode

contains
subroutine rk4(t0,y0,dt,nt,y)
    !4th order RK method
    implicit none
    real(kind=8), dimension(:), intent(in) :: y0
    real(kind=8), intent(in) :: t0,dt
	integer, intent (in) :: nt
    real(kind=8), dimension(size(y0)), intent(out) :: y
    real(kind=8), dimension(size(y0)) :: f1, f2, f3, f4
    real(kind=8) :: t,halfdt,fac
	integer:: k

        halfdt = 0.5d0*dt
        fac = 1.d0/6.d0

        y = y0
        t = t0

        do k = 1, nt

           f1 = dt*RHS(t, y)

           f2 = dt*RHS(t + halfdt, y + 0.5d0*f1)

           f3 = dt*RHS(t + halfdt, y + 0.5d0*f2)

           f4 = dt*RHS(t + dt, y + f3)

           y = y + (f1 + 2*f2  + 2*f3 + f4)*fac

           t = t + dt*dble(k)

        end do
end subroutine rk4
!------------------
subroutine euler(t0,y0,dt,nt,y)
    !explicit Euler method
    implicit none
    real(kind=8), dimension(:), intent(in) :: y0
    real(kind=8), intent(in) :: t0,dt
	integer, intent (in) :: nt
    real(kind=8), dimension(size(y0)), intent(out) :: y
    real(kind=8) :: t,halfdt,fac
	integer:: k

    y = y0
    t = t0
    do k = 1,nt

        y = y + dt*RHS(t,y)
        t = t + dt

    end do


end subroutine euler
!--------------------
subroutine euler_omp(t0,y0,dt,nt,numthreads,y)
    !explicit Euler method, parallelized with OpenMP
    !i achieved this parallelisation by using if statements to deal with the boundaries
    !the case where numthreads==1 runs the code just like it does in serial euler
    !the case where threadID==0 and threadID==numthreads-1 are the boundaries
    !in both cases dydx is collected using 2nd-ordered centered scheme with the
    !boundaries calculated seperately using f_N+1=f_1 and f_0=f_N since function
    !periodic, omp barrier is added to ensure all threads have computed dydx before
    !continuing
    !once the derivative is calculated, it is passed through RHS_omp to form the RHS
    use omp_lib
    use fdmodule
    implicit none
    real(kind=8), dimension(:), intent(in) :: y0
    real(kind=8), intent(in) :: t0,dt
	integer, intent (in) :: nt,numthreads
    real(kind=8), dimension(size(y0)), intent(out) :: y
    real(kind=8), dimension(size(y0)):: dydx
    real(kind=8) :: t,invtwodx
	integer:: k,threadID,istart,iend,ntot

!$  call omp_set_num_threads(numthreads)
    y = y0
    t = t0
    ntot = size(y)

!$omp parallel private(istart,iend,threadID)
    threadID = omp_get_thread_num()
    call mpe_decomp1d(ntot,numthreads,threadID,istart,iend) !construct domain decomposition
    print *, 'istart,iend,threadID=',istart,iend,threadID
    do k = 1,nt

    invtwodx = 0.5d0/dx
    if (numthreads==1) then
        dydx(istart+1:iend-1) = invtwodx*(y(istart+2:iend)-y(istart:iend-2))
        dydx(istart) = invtwodx*(y(istart+1) - y(n))
        dydx(iend) = invtwodx*(y(1) - y(iend-1))
    else if (threadID==0) then
        dydx(istart+1:iend) = invtwodx*(y(istart+2:iend+1)-y(istart:iend-1))
        !b.c.'s
        dydx(istart) = invtwodx*(y(istart+1) - y(n))
    else if (threadID==numthreads-1) then
        dydx(istart:iend-1) = invtwodx*(y(istart+1:iend)-y(istart-1:iend-2))
        !b.c.'s
        dydx(iend) = invtwodx*(y(1) - y(iend-1))
    else
        dydx(istart:iend) = invtwodx*(y(istart+1:iend+1)-y(istart-1:iend-1))
    end if
!$omp barrier
    !print *,RHS_omp(t,dydx(istart:iend))
        y(istart:iend) = y(istart:iend) + dt*RHS_omp(t,dydx(istart:iend)) !complete call to RHS_omp      
        t = t + dt
!$omp barrier
    end do
    !print *, dydx
    print *, 'finished loop:',threadID,maxval(abs(y(istart:iend))) !this will slow the code but is included for assessment.
!$omp end parallel

end subroutine euler_omp

!-----------------------
function RHS_omp(t,f)
    !called by euler_omp
    !RHS_omp = df/dt
    use advmodule
    use fdmodule
    implicit none
    integer :: i1
    real(kind=8), intent(in) :: t
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)) :: RHS_omp
    !add variable declaration for RHS_omp
    RHS_omp=S_adv-c_adv*f



 end function RHS_omp
!--------------------------------------

function RHS(t,f)
    !called by euler and rk4
    !RHS = df/dt
    use fdmodule
    use advmodule
    implicit none
    real(kind=8), intent(in) :: t
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)) :: RHS
    real(kind=8), dimension(size(f)) :: dfdx

    call fd2(f,dfdx) 
    RHS=S_adv-c_adv*dfdx


end function RHS
!-----------------
end module ode


!--------------------------------------------------------------------
!  (C) 2001 by Argonne National Laboratory.
!      See COPYRIGHT in online MPE documentation.
!  This file contains a routine for producing a decomposition of a 1-d array
!  when given a number of processors.  It may be used in "direct" product
!  decomposition.  The values returned assume a "global" domain in [1:n]
!
subroutine MPE_DECOMP1D( n, numprocs, myid, s, e )
    implicit none
    integer :: n, numprocs, myid, s, e
    integer :: nlocal
    integer :: deficit

    nlocal  = n / numprocs
    s       = myid * nlocal + 1
    deficit = mod(n,numprocs)
    s       = s + min(myid,deficit)
    if (myid .lt. deficit) then
        nlocal = nlocal + 1
    endif
    e = s + nlocal - 1
    if (e .gt. n .or. myid .eq. numprocs-1) e = n

end subroutine MPE_DECOMP1D

